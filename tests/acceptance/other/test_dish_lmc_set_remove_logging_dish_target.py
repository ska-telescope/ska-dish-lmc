# pylint: disable=W0511
"""
Verify setting and removal of SPF and SPFRx Dishlogger targets
"""
import pytest
import tango


# TODO : move the this test to unit test folder (Need to solve the connection error issue first)
@pytest.mark.lmc_logger
def test_set_and_remove_logger():
    """
    Test function to verify setting and removal of SPF and SPFRx Dishlogger targets
    """
    dish_logger = tango.DeviceProxy("mid-dish/dish-logger/SKA001")
    spf = tango.DeviceProxy("mid-dish/simulator-spfc/SKA001")
    spfrx = tango.DeviceProxy("mid-dish/simulator-spfrx/SKA001")

    # set the logging target
    dish_logger.SetDishLoggerTarget(spf.name())
    dish_logger.SetDishLoggerTarget(spfrx.name())

    dish_logger_device_name = f"device::{dish_logger.name()}"

    # check that it's there
    assert dish_logger_device_name.casefold() in spf.get_logging_target()
    assert dish_logger_device_name.casefold() in spfrx.get_logging_target()

    # remove the logging target
    dish_logger.RemoveDishLoggerTarget(spf.name())
    dish_logger.RemoveDishLoggerTarget(spfrx.name())

    # check that it's been removed
    assert dish_logger_device_name.casefold() not in spf.get_logging_target()
    assert dish_logger_device_name.casefold() not in spfrx.get_logging_target()
