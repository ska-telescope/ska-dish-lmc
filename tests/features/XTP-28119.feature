@VTS-226
Feature: Dish LMC acceptance tests
	
	@XTP-28119 @XTP-811 @XTP-16286 @L2-4696 @L2-4695 @L2-4651
	Scenario: Dish LMC Reports DSH Capability Unavailable
		Given dish_structure operatingMode reports STARTUP or ESTOP
		And spfrx b2CapabilityState reports UNAVAILABLE
		And spf b2CapabilityState reports UNAVAILABLE
		Then dish_manager b2CapabilityState should report UNAVAILABLE
