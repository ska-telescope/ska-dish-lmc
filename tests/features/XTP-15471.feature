@VTS-226
Feature: Dish LMC acceptance tests
	
	@XTP-15471 @XTP-811 @XTP-16286 @L2-4697 @L2-4695 @L2-4651
	Scenario: Dish LMC Reports DSH Capability Standby in FP mode
		Given dish_manager dishMode reports STANDBY-FP
		When I issue ConfigureBand2 on dish_manager
		Then spfrx b2CapabilityState should report OPERATE
		And spf b2CapabilityState should report OPERATE-FULL
		And dish_manager b2CapabilityState should report STANDBY
