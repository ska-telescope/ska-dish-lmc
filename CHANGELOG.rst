###########
Change Log
###########

All notable changes to this project will be documented in this file.
This project adheres to `Semantic Versioning <http://semver.org/>`_.

## unreleased
*************

Version 7.0.0
*************
- Updated dockerfile to use new base images and improved docker image build

- Added a chart dependency for the B5dc proxy Tango device (not deployed by default)
  
  - Deploys a new Tango device that acts as a proxy for the B5dc device
  - Deploys a new B5dc simulator device that simulates the B5dc device
  - Added a new engineering dashboard for the B5dc proxy Tango device

- Upgraded ska-mid-dish-simulators chart version to v4.3.0

  - Updated dockerfile to use new base images and improved docker image build
  - Fixed bug that sets reference table after stop track called

- Upgraded ska-mid-dish-ds-manager chart version to v3.1.1

  - Added Band0PointingModelParams
  - Updated dockerfile to use new base images and improved docker image build
  - Replaced component manager with a new implementation using sculib

- Upgraded ska-mid-dish-manager chart version to v7.0.0

  - Provided fix for SKB-751
  - Addressed failures on lrc_result resulting from missing attributes in SPFRx
  - Updated dockerfile to use new base images and improved docker image build
  - Implemented Band0PointingModelParams attribute

Version 7.0.0-rc2
*****************
- Cleaned up DSManager logs
- Not deploying B5DC devices by default

Version 7.0.0-rc1
*****************
- Updated dockerfile to use new base images and improved docker image build

- Added a chart dependency for the B5dc proxy Tango device
  
  - Deploys a new Tango device that acts as a proxy for the B5dc device
  - Deploys a new B5dc simulator device that simulates the B5dc device
  - Added a new engineering dashboard for the B5dc proxy Tango device

- Upgraded ska-mid-dish-simulators chart version to v4.3.0

  - Updated dockerfile to use new base images and improved docker image build
  - Fixed bug that sets reference table after stop track called

- Upgraded ska-mid-dish-ds-manager chart version to v3.1.0

  - Added Band0PointingModelParams
  - Updated dockerfile to use new base images and improved docker image build
  - Replaced component manager with a new implementation using sculib

- Upgraded ska-mid-dish-manager chart version to v7.0.0

  - Provided fix for SKB-751
  - Addressed failures on lrc_result resulting from missing attributes in SPFRx
  - Updated dockerfile to use new base images and improved docker image build
  - Implemented Band0PointingModelParams attribute

Version 6.0.1
*************
- Added `trackTableCurrentIndex` and `trackTableEndIndex` attributes that reflect track table space availability.
- Increased track table buffer availability by not copying zero-padded track table entries to internal buffer.
- Added validation check for `ApplyPointingModel` command.
- Updated ska-mid-dish-manager to v6.0.1
- Updated ska-mid-dish-ds-manager to v3.0.1
- Updated ska-mid-dish-simulators to v4.2.2

Version 6.0.0
*************
- Added spectrum graphing widgets to Dish LMC and DS Manager Taranta dashboards to plot achievedPointing
- Added instructions detailing configuration of Taranta dashboard variables 
- Updated ska-tango-util to v0.4.13
- Updated ska-tango-base to v0.4.13
  
- DishManager

  - Updated to v6.0.0
  - Added unit and range verification checks to `ApplyPointingModel`command
  - Added in Read/Write dscPowerLimitKw attribute to be used when SetStandbyFPMode, SetStandbyLPMode and Slew are invoked.
  - Upgraded ska-mid-dish-ds-manager chart to v3.0.0
  - Upgraded ska-mid-dish-simulators to v4.2.1
  - Updated SPFRx operatingMode from DATA_CAPTURE to OPERATE to match Rev 4 ICD
  - Added in Read/Write dscPowerLimitKw attribute to be used when FP, LP and Slew are invoked.
  - Updated periodicNoiseDiodePars and pseudoRandomNoiseDiodePars to be DevULong
  - Added `Abort` tango command which cancels any task and restores the dish to FP mode

    - `AbortCommmands` implements the same handler as `Abort`
  
- DSManager

  - Updated to v3.0.0
  - Updated simulators version to v4.2.1
  - Added in Read/Write dscPowerLimitKw attribute to be used when FP, LP and Slew are invoked.
  - Updated error message string to contain the error from OPCUA

- Simulators

  - Updated to v4.2.1
  - Updated SPFRx operatingMode from DATA_CAPTURE to OPERATE to match Rev 4 ICD
  - Updated SPFRx periodicNoiseDiodePars and pseudoRandomNoiseDiodePars to be DevULong
  - Corrected TiltOnType enum display names
  - Updated Stow elevation from science elevation limit to mechanical elevation limit

Version 5.0.1
*************
- DishManager

  - Updated to 5.0.1
  - Upgraded ska-mid-dish-simulators to v4.1.2

    - Servo loops simulator implemented to represent dish movement

  - Upgraded ska-mid-dish-ds-manger chart to v2.1.1

    - DSC states and modes updated to align with ITF PLC

  - Added in a command called ApplyPointingModel that updates bands using a JSON input
  - Added Slew command execution preconditions on DishMode and PointingState

    - NOTE DishMode required to be in OPERATE and PointingState required to be READY for Slew

  - Added an attribute called last_commanded_pointing_params that reports the last updated pointing parameters

- DSManager

  - Updated to 2.1.1
  - Added SetPowerMode tango command
  - DSC states updated to align with PLC in ITF
  - Changed TrackLoadTable to a FastCommand.
  - Added B5a and B5b pointing model param attributes
  - Pointing update interval was reverted to 50ms due to reduced overhead from recent log changes
  - Application and System nodes cached - reduce time.
  - Updated the ApplicationState helper function to default to nodeID since the ITF PLC has an issue with the BrowseName.
  - Makes us of TiltOnType enum value as specified in ICD.

- Simulators

  - Updated to 4.1.2
  - Account for padding in TrackLoadTable method on DSC sim
  - Change order of take auth return values
  - Update DSC TiltOnType enum values from One/Two to TiltmeterOne/TiltmeterTwo
  - Added motion control loop module for dish movement simulation
  - Relaxed DSC state to standstill if either azimuth or elevation axes are in standstill
  - Updated ska-tango-base and ska-tango-util to version 0.4.12
  - Now starting the dish pointed at Polaris Australis (181, 31)

Version 4.2.0
*************
- Updated buildState attribute on Dish Logger to report the name and version of the Dish LMC package
- Updated EDA configuration
- Developer docs enhancements which include:

  - Revised formatting
  - The addition of a notebook that walks readers through exercising dish pointing functionality
  - The expansion of the Dish LMC API to include the new release data retrieval interface

- Added README with Taranta dashboard setup guide and screenshots
- Added Dish LMC device version notebook
- Upgraded ska-mid-dish-manager chart version to v4.0.0:

  - Updated `buildState` attribute to include version information of dish manager and subservient devices
  - Upgraded ska-mid-dish-simulators chart to v4.0.1
  - Upgraded ska-mid-ds-manager version to v2.0.0
  - Added actStaticOffsetValueXel and actStaticOffsetValueEl attributes
  - Updated band<N>PointingModelParams usage
  - Added `lastCommandedMode` attribute to record the last mode change request
  - Removed achievedPointingAz and achievedPointingEl
  - Fixed missing events from sub-devices on the event consumer thread
  - Exposed noide diode attributes from SPFRx:

    - noiseDiodeMode, periodicNoiseDiodePars, pseudoRandomNoiseDiodePars

- Upgraded ska-mid-dish-ds-manager chart version to v2.0.0

  - Changed OPCUA method call exception handling to handle all UA related exceptions
  - Updated to align with dish structure controller ICD v0.0.10
  - Migrated remaining OPCUA method calls to use updated argument signature which requires session id
  - HealthState mapping triggered by CurrentMode and Application State nodes
  - Removed achievedPointingAz and achievedPointingEl attributes.
  - Added currentPointing attribute.
  - Updated the logic to use a subset of currentPointing for the achievedPointing attribute.

- Upgraded ska-mid-dish-simulators chart version to v4.0.1

  - Enable change events for SPFRx attributes
    - noiseDiodeMode, periodicNoiseDiodePars, pseudoRandomNoiseDiodePars
  - Return CommandDone for DS Move2Band if already in given band

Version 4.1.0
*************
- Updated the Stow Command to execute immediately when triggered and to abort all queued LRC tasks afterwards
- Updated Dish Structure Manager to align with Dish Structure Controller ICD v0.0.7
- Implemented session ID management for Dish Structure Controller
- Implemented ability to Append to track table while actively Tracking
- Resolved SKB-443

  - Changed Dish Structure Manager healthState to report OK when init is complete
  - Changed Dish Structure Manager State to report RUNNING when init is complete

- Updated package dependencies:

  - Updated ska-tango-base to v1.0.0
  - Updated PyTango to v9.5.0

- Updated chart dependencies:

  - ska-mid-dish-manager to 3.0.1
  - ska-mid-dish-ds-manager to 1.5.0
  - ska-mid-dish-simulators to 3.1.0

- WARNING: writes to `band[X]PointingModelParams` fails due to data type mismatch in current OPCUA nodeset file
   
Version 4.0.0
*************
- Updated ska-mid-dish-manager chart version to v3.0.0

  - DishManager uses v1.0 of the base classes
  - DishManager performs "is command allowed" check on dequeue operation
  - DishManager `longRunningCommandInProgress` reports more than one lrc in progress i.e. from itself and subservient devices

Version 3.4.0
*************
- Added more exhaustive per command logging
- Updated to use SKA epoch for TAI timestamps to address SKB-397
- Updated chart dependencies:

  - ska-mid-dish-manager to 2.7.0
  - ska-mid-dish-ds-manager to 1.4.1
  - ska-mid-dish-simulators to 2.0.5

Version 3.3.0
*************
- Increased L2 requirements test coverage
- Added tests to check SPFRx interface (SKB-313)
- Upgraded  ska-tango-util chart version to v0.4.11
- Upgraded ska-tango-base chart version to v0.4.10
- Removed SPFRx tango-db registration responsibility from Dish LMC to SPFRx deployer (v0.2.2)
- Provided alternative deployment of Dish LMC using docker compose for MeerKAT CAM (DVS integration)

- General test improvements

  - Updated pytest markers to acceptance and smoke_test only
  - Removed usage of custom reset commands on spf/spfrx simulators
  - Updated docs to socialise order of test job execution in integration exercises
  - Added pipeline variable to switch between resets or no resets during tests execution

- Upgraded ska-mid-dish-manager chart version to v2.6.0

  - Removed lmc tests and its manual job trigger
  - Disabled default deployment of DSManager to use helm flag
  - Added ignoreSpf and ignoreSpfrx attributes to conform to ADR-93
  - Updated command map and transition state rules for when ignoring spf/spfrx to conform to ADR-93
  - Removed azimuth and elevation speed arguments from Slew command
  - Added quality state callback to publish change event on subservient device attribute quality changes
  - Resolved a bug raised on setting the kValue on the SPFRx (SKB-356)
  - Added configureTargetLock implementation
  - Updated implementation of pointing model parameters for bands 1, 3 and 4
  - Added tests for aborting of long running commands

- Upgraded ska-mid-dish-ds-manager to 1.3.1

  - Update docs to demonstrate running devices as nodb
  - Added attributes azimuthSpeed and elevationSpeed used in slew command
  - Removed explicity call to activate and deactivate axes on STANDBY_LP and STANDBY_FP
  - Added TrackProgramMode attribute to be used for selecting TABLE or POLYNOMIAL tracking
  - Refactored DS manager OPC-UA subscriptions callback
  - Reduced OPC-UA subscriptions from over 20 to 3
  - Grouped subscriptions into 3: Pointing, Static and Dynamic
  - Added tango attributes achievedTargetLock and configureTargetLock
  - Update TAI conversion in source timestamp conversion and track start time

- Upgraded ska-mid-dish-simulators chart version to v2.0.4

  - Update DS OPC-UA simulator Track start validation
  - Added setter/getter test methods to SPFRx simulator for attenuationPolV attribute quality

Version 3.2.0
*************
- Added engineering dashboards for DSManager, SPF and SPFRx devices
- Upgraded ska-tango-base chart version to v0.4.9
- Upgraded ska-mid-dish-manager chart version to v2.5.0 

  - Updated the Scan command to record scanID from TMC.
  - Added EndScan command to clear scanID attribute.
  - Added scanID attribute (read/write) to set/unset the scanID
  - Removes desiredPointing attribute
  - Exposes desiredPointingAz and desiredPointingEl attributes

Version 3.1.0
*************
- Updated dish manager docs to demonstrate running devices as nodb
- Added `MonitoringPing` command to the interface to continually call `MonitorPing` on SPFRx to indicate that it is connected to Dish.LMC
- Implemented a workaround to fix segfault errors in dish manager python-test job
- Updated dish simulators version to v1.6.6
- Updated dish-manager version to v2.4.0
- Resolved SKB-292 and SKB-293 by retrying connections to the OPCUA server until we are successful

Version 3.0.0
*************
- Added EDA configuration file to archive LMC attributes
- Update Dish LMC device names to conform to ADR-9

  - Before Version 3.0.0: ska001/elt/master
  - After Version 3.0.0: mid-dish/dish-manager/SKA001

Version 2.6.3
*************
- Extended smoke tests to cover interface checks for DS and SPFRx
- Fleshed out missing content in documentation
- Upgraded ska-mid-dish-simulators chart version to v1.6.4
- Upgraded ska-tango-util chart version to v0.4.10
- Upgraded ska-tango-base chart version to v0.4.8
- Upgraded ska-mid-dish-manager chart version to v2.3.5

  - Includes ResultCode in updates to longRunningCommandResult

Version 2.6.2
*************
- Use ska-ser-sphinx-theme for documentation
- Fix dish naming for dish IDs more than 100
- Upgraded ska-mid-dish-manager chart version to v2.3.3
- Upgraded ska-mid-dish-ds-manager chart version to v1.2.3
- Upgraded ska-mid-dish-simulators chart version to v1.6.2

Version 2.6.1
*************
- Removed all legacy C++ source and related dependencies
- Explicitly convert dish IDs to strings in template
- Upgraded ska-mid-dish-manager chart version to v2.3.2
- Upgraded ska-mid-dish-ds-manager chart version to v1.2.2
- Upgraded ska-mid-dish-simulators chart version to v1.6.1

Version 2.6.0
*************
- Updated dockerfile to remove C++ code compilation and build for only Python
- General test improvements, notably on event callback store
- Upgraded ska-mid-dish-manager chart version to v2.3.1
- Upgraded ska-mid-dish-simulators chart version to v1.6.0
- Upgraded ska-tango-util chart version to v0.4.9
- Updated README and helm chart flags
- Added fixture across tests to record events in k8s test run
- Updated TARANTA dashboard
- Updated test stage to deploy and test only Python DS Manager

  - C++ DS Manager is no longer deployed
  - `desiredPointing` on OPCUA server is being worked on and may not function as expected
  - Tests writing to desired pointing have been skipped until additional work in OPCUA server is completed

Version 2.5.0
*************
- Documentation updates
- Upgraded to ska-tango-util v0.4.6 chart version
- Added ska-mid-dish-ds-manager v0.0.2 chart version
- Upgraded to ska-mid-dish-manager v2.2.9 chart version
- Added manual job to test stage to execute acceptance test against DS Manager

Version 2.4.0
*************
- Added manual jobs to deploy Dish LMC to integrate with SPF Controller in the ITF
- Upgraded to DishManager v2.2.8 chart version

  - New helm flags for configurable device deployments

Version 2.3.2
*************
- Fixed the docs build.
- Updated the Readme file.

Version 2.3.1
*************
- Bumped dishmanager chart version to 2.2.4

Version 2.3.0
*************
- Updated DishManager chart to v2.2.3 using non-async simulator devices
- Added acceptance for testing log forwarding

Version 2.2.0
*************
- Updated DishManager chart to v2.2.2

Version 2.1.1
*************
- Updated tests to match DishManager interface
- Updated DishManager chart to v2.1.2

Version 2.1.0
*************
- Added Taranta dependency to the helm charts.
- Added the ability to enable and disable Taranta dashboard during a deployment.
- Tango device names conforms to ADR-32 dish ID format e.g mid_d001/lmc/ds_simulator -> ska001/lmc/ds_simulator
- Updated dish manager version to 2.1.0

Version 2.0.0
*************
- First version with all BDD tests passing
- Updated dishmanager chart
- Updated testware to improve test robustness
- Reworked BDD tests to fix failures

Version 1.4.0
*************
- Updated DS enums
- Updated dishmanager chart
- Updated BDD tests

Version 1.3.0
*************
- Update SPFRx to replace STANBY-LP/STANBY-FP with STANDBY

Version 1.2.0
*************
- Replaced LMCLogger with DishLogger
- Configured LMC devices to log to DishLogger

Version 1.1.0
*************
- Slimmed down docker container size
- Added tests
- Updated Helm chart to allow for customised deployments

Version 1.0.0
*************
- Initializing project.
