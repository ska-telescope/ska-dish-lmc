# pylint: disable=invalid-name
"""Release information for Dish LMC package."""

from importlib.metadata import PackageNotFoundError, version

name = "Dish LMC"
DISH_LMC_PACKAGE_NAME = "ska-dish-lmc"


def get_dish_lmc_release_version() -> str:
    """Get release version of package."""
    try:
        release_version = version(DISH_LMC_PACKAGE_NAME)
    except PackageNotFoundError:
        release_version = f"ERR: parsing {DISH_LMC_PACKAGE_NAME} version."
    return release_version
