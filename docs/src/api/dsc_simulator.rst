===================================
Dish Structure Controller Simulator
===================================

* `DSC Simulator Device Server`_
* `DSC Simulator Interface Description`_

.. _DSC Simulator Device Server: https://developer.skao.int/projects/ska-mid-dish-simulators/en/latest/api/devices/ds_opcua_server.html
.. _DSC Simulator Interface Description: https://developer.skao.int/projects/ska-mid-dish-simulators/en/latest/api/interface/ds_spec.html
