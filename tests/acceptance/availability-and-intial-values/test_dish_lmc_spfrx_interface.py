"""Tests the interface between Dish LMC and SPFRx inside of the ITF"""

import pytest
import tango.utils
from tango import CmdArgType, EventType


@pytest.mark.smoke_test
@pytest.mark.parametrize(
    "spfrx_current_attr",
    [
        "attenuationPolH",
        "attenuationPolV",
        "kValue",
        "b1CapabilityState",
        "b2CapabilityState",
        "b3CapabilityState",
        "b4CapabilityState",
        "b5aCapabilityState",
        "b5bCapabilityState",
        "operatingMode",
        "healthState",
        "configuredBand",
        "capturingData",
    ],
)
def test_verify_availabiliy_of_all_spfrx_attributes(spfrx_current_attr, spfrx):
    """Test to verify the availability of SPFRx attributes expected"""
    spfrx_attr_list = spfrx.get_attribute_list()
    # Check that the current attributes is contained in the list of test attributes
    assert spfrx_current_attr in spfrx_attr_list


@pytest.mark.smoke_test
@pytest.mark.parametrize(
    "spfrx_current_cmd",
    [
        "SetKValue",
        "SetStandbyMode",
        "ConfigureBand1",
        "ConfigureBand2",
        "ConfigureBand3",
        "ConfigureBand4",
        "ConfigureBand5a",
        "ConfigureBand5b",
    ],
)
def test_verify_availabiliy_of_all_spfrx_commands(spfrx_current_cmd, spfrx):
    """Test to verify the availability of the spfrx commands expected"""
    spfrx_command_list = spfrx.get_command_list()
    # Check that the current command is contained in the returned command list
    assert spfrx_current_cmd in spfrx_command_list


@pytest.mark.smoke_test
@pytest.mark.parametrize(
    "spfrx_current_attr",
    [
        "attenuationPolH",
        "attenuationPolV",
        "kValue",
        "b1CapabilityState",
        "b2CapabilityState",
        "b3CapabilityState",
        "b4CapabilityState",
        "b5aCapabilityState",
        "b5bCapabilityState",
        "operatingMode",
        "healthState",
        "configuredBand",
        "capturingData",
    ],
)
def test_attribute_event_subscription(spfrx, spfrx_current_attr):
    """Test to verify that change events are setup on spfrx attributes"""
    try:
        event_id = spfrx.subscribe_event(
            spfrx_current_attr, EventType.CHANGE_EVENT, tango.utils.EventCallback()
        )
        spfrx.unsubscribe_event(event_id)
        assert True
    except tango.DevFailed as ex:
        reasons = {dev_error.reason for dev_error in ex.args}
        pytest.fail(
            "Exceptions thrown on " + spfrx_current_attr + " event subscription: " + str(reasons),
            False,
        )


@pytest.mark.smoke_test
def test_operating_mode_attribute_type(spfrx):
    """Test to verify operatingMode attr defined as DevEnum"""
    attribute_type = spfrx.read_attribute("operatingMode").type
    assert attribute_type == CmdArgType.DevEnum
