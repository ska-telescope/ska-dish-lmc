# pylint: disable=C0301,C0116
"""Verify that dish transitions to STOW on comms failure to TMC

jira requirement: L2-4600
"""

import logging
import time

import pytest
import tango
from pytest_bdd import given, scenario, then, when
from pytest_bdd.parsers import parse
from utils import retrieve_attr_value

LOGGER = logging.getLogger(__name__)

# elevation drive rate is from the simulator behaviour
ELEV_DRIVE_MAX_RATE = 1.0
TOLERANCE = 1e-2


@pytest.mark.xfail(reason="DishManager yet to implement this capability")
@pytest.mark.acceptance
@scenario("XTP-28438.feature", "Dish LMC stows on TMC comms failure")
def test_dish_stows_on_comms_failure(monitor_tango_servers):
    pass


@given(parse("dish_manager dishMode is not {stow}"))
def dish_reports_allowed_dish_mode(stow, dish_manager):
    current_dish_mode = retrieve_attr_value(dish_manager, "dishMode")
    assert current_dish_mode != stow


@when(parse("dish_manager loses connection to TMC after {duration:g} minutes"))
def check_dish_manager_verifies_tmc_connection(duration, dish_manager, event_store_class):
    # TODO (ST 04/2024): dish_manager does not have this capability at the moment
    # The idea will be to have a thread (or polled command) check an attribute within
    # the time period. The attribute can hold a timestamp of when it was last updated
    # and another attribute will indicate (and archive) whether comms is alive or dead

    # set the timestamp to 6 mins in the past
    last_update_time = time.time() - 360
    dish_manager.lastPingUpdate = last_update_time

    event_cb = event_store_class()
    dish_manager.subscribe_event(
        "isTmcCommsAlive",
        tango.EventType.CHANGE_EVENT,
        event_cb,
    )
    duration *= 60
    event_cb.get_queue_values(timeout=duration)
    assert not dish_manager.isTmcCommsAlive


@then("dish_manager should issue a stow request")
def check_dish_manager_issued_a_stow(
    event_store_class,
    dish_manager,
):
    event_cb = event_store_class()
    dish_manager.subscribe_event(
        "longRunningCommandProgress",
        tango.EventType.CHANGE_EVENT,
        event_cb,
    )
    event_cb.wait_for_progress_update("Stow completed")


@then(parse("dish_manager dishMmode should report {stow}"))
def check_dish_manager_has_stowed(
    stow,
    dish_manager,
):
    current_dish_mode = retrieve_attr_value(dish_manager, "dishMmode")
    assert current_dish_mode == stow
