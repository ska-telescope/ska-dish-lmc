===================
Dish LMC Simulators
===================

DS Controller
-------------

The DS Controller is an OPC-UA server that organises information into nodes in an address
space. The address space is accessed using a client that connects to the URL of the server.

SPFC simulator
--------------

The SPFC simulator is tango device which simulates the Single Pixel Feed Controller.

SPFRx simulator
---------------

The SPFRx simulator is tango device which simulates the Single Pixel Feed Receiver Controller.

For more context on the dish simulators explore the project `documentation`_.

.. _documentation: https://developer.skao.int/projects/ska-mid-dish-simulators/en/latest/?badge=latest
