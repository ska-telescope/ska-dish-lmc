======================
Dish Structure Manager
======================

* `Dish Structure Manager Device Server`_
* `Dish Structure Manager Interface Description`_

.. _Dish Structure Manager Device Server: https://developer.skao.int/projects/ska-mid-dish-ds-manager/en/latest/api/DSManager.html
.. _Dish Structure Manager Interface Description: https://developer.skao.int/projects/ska-mid-dish-ds-manager/en/latest/api/ds_manager_interface.html
