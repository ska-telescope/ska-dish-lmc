## Convert Jupyter notebooks to _rst_

### Install dependencies

```bash
pip3 install jupyter
apt-get update
apt-get install pandoc
```

### Convert to rst
```bash
jupyter nbconvert --to rst <notebook_file>
```

### Convert to html
```bash
jupyter nbconvert --to html --template classic  notebooks/pointing.ipynb
```
