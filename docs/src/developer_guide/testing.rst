=======
Testing
=======

The ska-dish-lmc repository integrates all the sub components under one umbrella to verify
it against a set of L2 requirements. All the requirements have been extracted from JAMA and
written into a set of `BDD tests`_ which exercise the dish control and monitoring. These tests
are run for every merged change and uploaded to xray (see `example test execution`_) in the
``xray_publish`` job for every release. 

Unit Test
^^^^^^^^^

This runs a set of tests against the DishLogger (using the ``DeviceTestContext``) to ensure
that logs are sent to ``stdout`` according to the `SKA logging format`_. Tests output are
captured in the ``python-test`` job.

Acceptance Tests
^^^^^^^^^^^^^^^^

This deploys the dish lmc tango devices with the tango dB to run the acceptance tests.
These tests use the `simulated devices`_ when run in the k8s cluster. The simulated devices
for the ``SPF Controller``, ``SPFRx Controller`` and the ``DS Simulator`` have limited functionality
as they do not emulate the hardware entirely. All tests that are **not** marked as ``unit`` or has
postfix ``_itf`` or ``_eda`` will run in the k8s cluster with `simulated devices`_.

Each test session has an event recorder which stores attribute events for the devices as 
an artefact; this is very useful for debugging. The acceptance tests are captured in the 
``k8-test-runner`` job.

Component (Hardware) Integration
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The acceptance tests are tests that are connected to `BDD tests`_ are required to run against
the actual hardware. A prerequisite to running the integration test suite against the hardware
deployed in the Integrated Test Facility (ITF), is to run the smoke tests to verify that the
devices implement the expected interface. The smoke can be run using the ``smoke_test`` marker.
By using the marker ``acceptance`` in the associated test, the test will run on hardware in 
the ITF.

.. _simulated devices: https://gitlab.com/ska-telescope/ska-mid-dish-simulators
.. _BDD tests: https://jira.skatelescope.org/browse/XTP-811
.. _example test execution: https://jira.skatelescope.org/browse/XTP-29731
.. _SKA logging format: https://developer.skao.int/en/latest/tools/logging-format.html
