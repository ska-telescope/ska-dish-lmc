@VTS-226
Feature: Dish LMC acceptance tests
	
	@XTP-28438 @XTP-811 @XTP-16286 @L2-4600
	Scenario: Dish LMC stows on TMC comms failure
		Given dish_manager dishMode is not STOW
		When dish_manager loses connection to TMC after 5 minutes
		Then dish_manager should issue a STOW request
		And dish_manager dishMmode should report STOW
