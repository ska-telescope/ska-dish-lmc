## Jupyter notebook instructions

### Mid ITF

* For MID ITF, log in at https://k8s.miditf.internal.skao.int/binderhub/jupyterhub/
* Start a server
* Upload notebooks from here

Remember to update the namespace, cluster domain etc. as required. Alternatively, upload and use the `setup_env.py` file which defines the `init_env` helper method for setting up necessary environment variables and returns a dictionary of the device proxies. 

### Local Minikube 

#### Install the jupyterlab chart

```bash
helm repo add jupyterhub https://hub.jupyter.org/helm-chart/
helm repo update
helm upgrade --cleanup-on-fail --install jup jupyterhub/jupyterhub --namespace <NAMESPACE>
```

#### Forward the port
```bash
kubectl port-forward svc/proxy-public 8080:80 -n <NAMESPACE>
```

##### Navigate to http://localhost:8080/ and log in with any credentials

