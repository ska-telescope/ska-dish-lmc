"""
Test to check formatting of device properties of components.
"""

import pytest
import tango


@pytest.mark.parametrize("dish_number", ["001", "111"])
def test_device_manager_dish_id(dish_number):
    """Test format of DishId property."""
    dm_dp = tango.DeviceProxy(f"mid-dish/dish-manager/SKA{dish_number}")
    assert dm_dp.get_property("DishId")["DishId"][0] == f"SKA{dish_number}"


@pytest.mark.parametrize("dish_number", ["001", "111"])
def test_ds_manager_device_fqdn(dish_number):
    """Test format of DSDeviceFqdn property."""
    dm_dp = tango.DeviceProxy(f"mid-dish/dish-manager/SKA{dish_number}")
    assert (
        dm_dp.get_property("DSDeviceFqdn")["DSDeviceFqdn"][0]
        == f"mid-dish/ds-manager/SKA{dish_number}"
    )


@pytest.mark.parametrize("dish_number", ["001", "111"])
def test_sfp_device_fqdn(dish_number):
    """Test format of SPFDeviceFqdn property."""
    dm_dp = tango.DeviceProxy(f"mid-dish/dish-manager/SKA{dish_number}")
    assert (
        dm_dp.get_property("SPFDeviceFqdn")["SPFDeviceFqdn"][0]
        == f"mid-dish/simulator-spfc/SKA{dish_number}"
    )


@pytest.mark.parametrize("dish_number", ["001", "111"])
def test_sfprx_device_fqdn(dish_number):
    """Test format of SPFRxDeviceFqdn property."""
    dm_dp = tango.DeviceProxy(f"mid-dish/dish-manager/SKA{dish_number}")
    assert (
        dm_dp.get_property("SPFRxDeviceFqdn")["SPFRxDeviceFqdn"][0]
        == f"mid-dish/simulator-spfrx/SKA{dish_number}"
    )
