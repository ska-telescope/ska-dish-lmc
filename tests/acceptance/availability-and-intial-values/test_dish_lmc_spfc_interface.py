"""Tests the interface between Dish LMC and SPFC in of the ITF"""

import pytest


@pytest.mark.smoke_test
@pytest.mark.parametrize(
    "spf_current_attr",
    [
        "operatingMode",
        "powerState",
        "healthState",
        "bandInFocus",
        "b1CapabilityState",
        "b2CapabilityState",
        "b3CapabilityState",
        "b4CapabilityState",
        "b5aCapabilityState",
        "b5bCapabilityState",
    ],
)
def test_verify_availabiliy_of_all_spfc_attributes(spf_current_attr, spf):
    """Test to verify the availability of SPFC attributes expected"""
    spf_attr_list = spf.get_attribute_list()
    # Check that the current attribute is contained in the list of attributes
    assert spf_current_attr in spf_attr_list


@pytest.mark.smoke_test
@pytest.mark.parametrize(
    "spf_current_cmd",
    [
        "SetStandbyLPMode",
        "SetOperateMode",
        "SetMaintenanceMode",
        "Shutdown",
        "Restart",
        "SetFeedMode",
        "SendFeedCommand",
        "SetVacuumMode",
        "SetHeliumMode",
        "SendHeliumCommand",
        "RemountSdCard",
    ],
)
def test_verify_availabiliy_of_all_spfc_commands(spf_current_cmd, spf):
    """Test to verify the availability of the SPFC commands expected"""
    spf_command_list = spf.get_command_list()
    # Check that the current command is contained in the returned command list
    assert spf_current_cmd in spf_command_list
