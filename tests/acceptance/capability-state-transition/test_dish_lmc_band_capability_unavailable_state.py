# pylint: disable=W0107,C0116
"""
Verify that LMC reports Band_X Capability as UNAVAILABLE state
if Dish Structure is in STARTUP or ESTOP mode

jira requirement: L2-4696, L2-4695, L2-4651
"""
import logging

import pytest
from pytest_bdd import given, scenario, then
from pytest_bdd.parsers import parse
from utils import retrieve_attr_value

LOGGER = logging.getLogger(__name__)


@pytest.mark.xfail(reason="ESTOP/STARTUP cannot be request via DISH-LMC")
@pytest.mark.acceptance
@scenario(
    "XTP-28119.feature",
    "Dish LMC Reports DSH Capability Unavailable",
)
def test_dish_manager_capability_state_reports_unavailable(monitor_tango_servers):
    """Test that dish lmc reports UNAVAILABLE capability state"""
    pass


@given(parse("dish_structure operatingMode reports {startup} or {estop}"))
def check_dish_structure_operating_mode(dish_structure, startup, estop):
    # estop is not activated via Dish LMC.
    # See Dish LMC - Dish Structure Interface document in ICD
    # TODO (ST 04/2024): Investigate how to manipulate the dsc
    # simulator to transition DS Manager to report STARTUP or ESTOP

    assert dish_structure.operatingMode in [startup, estop]


@given(parse("spfrx b{band_number}CapabilityState reports {expected_state}"))
def check_spfrx_capability_state(band_number, expected_state, spfrx):
    b_x_capability_state = retrieve_attr_value(spfrx, f"b{band_number}CapabilityState")
    assert b_x_capability_state == expected_state


@given(parse("spf b{band_number}CapabilityState reports {expected_state}"))
def check_spf_capability_state(band_number, expected_state, spf):
    band_number = 5 if band_number.startswith("5") else band_number
    b_x_capability_state = retrieve_attr_value(spf, f"b{band_number}CapabilityState")
    assert b_x_capability_state == expected_state


@then(parse("dish_manager b{band_number}CapabilityState should report {expected_state}"))
def check_dish_capability_state(band_number, expected_state, dish_manager):
    b_x_capability_state = retrieve_attr_value(dish_manager, f"b{band_number}CapabilityState")
    assert b_x_capability_state == expected_state
