# pylint: disable=missing-function-docstring
"""
Verify that the startup values for dish and the subelements are accurate

jira requirement: L2-4633 L2-4691
"""

import logging

import pytest
import tango
from dish_enums import DishMode
from pytest_bdd import given, scenario, then
from pytest_bdd.parsers import parse
from utils import retrieve_attr_value

LOGGER = logging.getLogger(__name__)


def check_device_startup_mode(dev_proxy, desired_mode, attr="operatingMode"):
    desired_mode = desired_mode.replace("-", "_")
    actual_operating_mode = retrieve_attr_value(dev_proxy, attr)
    assert actual_operating_mode.replace("-", "_") == desired_mode
    LOGGER.info(f"{dev_proxy} operatingMode: {actual_operating_mode}")


@pytest.mark.acceptance
@scenario("XTP-13924.feature", "Test Dish LMC Dish Startup (LP Mode)")
def test_dish_lmc_startup(monitor_tango_servers):
    pass


@given(parse("dish_structure operatingMode reports {operating_mode}"))
def check_dish_structure_startup_operating_mode(operating_mode, dish_structure):
    check_device_startup_mode(dish_structure, operating_mode)


@given(parse("spf operatingMode reports {operating_mode}"))
def check_spf_startup_operating_mode(operating_mode, spf):
    check_device_startup_mode(spf, operating_mode)


@given(parse("spfrx operatingMode reports {operating_mode}"))
def check_spfrx_startup_operating_mode(operating_mode, spfrx):
    check_device_startup_mode(spfrx, operating_mode)


@then(parse("dish_manager dishMode reports {dish_mode}"))
def check_dish_manager_startups_with_expected_dish_mode(
    dish_mode, event_store_class, dish_manager
):
    """Test that dish manager starts up with the expected dishMode"""
    event_cb = event_store_class()
    dish_manager.subscribe_event(
        "dishMode",
        tango.EventType.CHANGE_EVENT,
        event_cb,
    )
    event_cb.wait_for_value(DishMode.STANDBY_LP)
    check_device_startup_mode(dish_manager, dish_mode, "dishMode")
