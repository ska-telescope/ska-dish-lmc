========
Pointing
========

This notebook and more can be downloaded from `from the repository notebook directory`_

.. raw:: html
   :file: pointing.html

.. _from the repository notebook directory: https://gitlab.com/ska-telescope/ska-dish-lmc/-/tree/master/notebooks?ref_type=heads
