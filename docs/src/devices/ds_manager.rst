==========
DS Manager
==========

This device exposes a tango interface to control and monitor the Dish Structure Simulator
which implements an OPCUA server. DS Manager is a surbsevient device to Dish Manager.

For more context on the dish manager explore the project `documentation`_.

.. _documentation: https://developer.skao.int/projects/ska-mid-dish-ds-manager/en/latest/?badge=latest
