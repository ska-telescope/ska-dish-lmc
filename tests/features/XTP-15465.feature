@VTS-226
Feature: Dish LMC acceptance tests
	
	@XTP-15465 @XTP-811 @XTP-16286 @L2-4635 @L2-4636 @L2-4649 @L2-4688 @L2-4691 @L2-4630
	Scenario: Test STANDBY-LP to STANDBY-FP
		Given dish_manager dishMode reports STANDBY-LP
		When I issue SetStandbyFPMode on dish_manager
		Then dish_manager dishMode should report STANDBY-FP
		And dish_structure operatingMode and powerState should report STANDBY-FP and FULL-POWER
		And spf operatingMode and powerState should report OPERATE and FULL-POWER
		And spfrx operatingMode should report either OPERATE or STANDBY
