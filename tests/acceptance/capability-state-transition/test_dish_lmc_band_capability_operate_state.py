# pylint: disable=C0116
"""
Verify that LMC reports Band X Capability as OPERATE

jira requirement: L2-4699, L2-4695, L2-4651
"""

import logging

import pytest
import tango
from dish_enums import CapabilityStates, SPFCapabilityStates, SPFRxCapabilityStates
from pytest_bdd import given, scenario, then, when
from pytest_bdd.parsers import parse
from utils import retrieve_attr_value

LOGGER = logging.getLogger(__name__)


@pytest.mark.acceptance
@scenario("XTP-6271.feature", "Dish LMC Reports DSH Capability Operate")
def test_band_capability_state_operate(monitor_tango_servers, init_devices):
    pass


@given(parse("dish_manager dishMode reports {dish_mode} for band{band_number} configuration"))
def check_dish_manager_dish_mode(
    dish_mode,
    band_number,
    spf,
    dish_manager,
    modes_helper,
):
    band_number = 5 if band_number.startswith("5") else band_number
    modes_helper.ensure_dish_manager_mode(dish_mode)
    current_dish_mode = retrieve_attr_value(dish_manager, "dishMode")
    LOGGER.info(f"{dish_manager} dishMode: {current_dish_mode}")


@given(parse("dish_structure operatingMode reports {operating_mode}"))
def check_dish_structure_operating_mode(operating_mode, dish_structure):
    current_operating_mode = retrieve_attr_value(dish_structure, "operatingMode")
    assert current_operating_mode == operating_mode
    LOGGER.info(f"{dish_structure} operatingMode: {current_operating_mode}")


@when(parse("I issue ConfigureBand{band_number} on dish_manager"))
def configure_band(
    band_number,
    dish_freq_band_configuration,
    spf,
    spfrx,
    dish_manager,
):
    dish_freq_band_configuration.go_to_band(band_number)

    dish_mode = retrieve_attr_value(dish_manager, "dishMode")
    if dish_mode == "STOW":
        if "simulator" in spf.name():
            # this command is available only on the simulator API
            # it's a convenience command to trigger capabilityState
            # change when band is configured in STOW dish mode
            spf.SetCapStateDegraded(1)


@then(parse("dish_structure indexerPosition should report {band_number}"))
def check_ds_indexer_position_is_not_moving(
    band_number,
    dish_structure,
):
    band_number = 5 if band_number.startswith("5") else band_number
    indexer_position = retrieve_attr_value(dish_structure, "indexerPosition")
    assert indexer_position == f"B{band_number}"
    LOGGER.info(f"{dish_structure} indexerPosition: {indexer_position}")


@then(parse("spf b{band_number}CapabilityState should report {expected_state}"))
def check_spf_band_capability_state(
    band_number,
    expected_state,
    spf,
    event_store_class,
    spfc_sim_mode_transition,
):
    # convert expected state to have underscore
    # for SPFCapabilityStates OPERATE_FULL enum
    expected_state = expected_state.replace("-", "_")

    spf_cap_state_events = event_store_class()

    spf.subscribe_event(
        f"b{band_number}CapabilityState",
        tango.EventType.CHANGE_EVENT,
        spf_cap_state_events,
    )

    if "b2LnaPidPowerState" in spf.get_attribute_list():
        spfc_sim_mode_transition.spfc_fp_workaround()
    else:
        spf_cap_state_events.wait_for_value(SPFCapabilityStates[expected_state], timeout=20)
        band_number = 5 if band_number.startswith("5") else band_number

    b_x_capability_state = retrieve_attr_value(spf, f"b{band_number}CapabilityState")
    assert b_x_capability_state.replace("-", "_") == expected_state
    LOGGER.info(f"{spf} b{band_number}CapabilityState: {b_x_capability_state}")


@then(parse("spfrx b{band_number}CapabilityState should report {expected_state}"))
def check_spfrx_capability_state(band_number, expected_state, spfrx, event_store_class):
    rx_cap_state_events = event_store_class()
    spfrx.subscribe_event(
        f"b{band_number}CapabilityState",
        tango.EventType.CHANGE_EVENT,
        rx_cap_state_events,
    )
    rx_cap_state_events.wait_for_value(SPFRxCapabilityStates[expected_state])
    b_x_capability_state = retrieve_attr_value(spfrx, f"b{band_number}CapabilityState")
    assert b_x_capability_state == expected_state
    LOGGER.info(f"{spfrx} b{band_number}CapabilityState: {b_x_capability_state}")


@then(parse("dish_manager b{band_number}CapabilityState should report {expected_state}"))
def check_dish_manager_band_capability_state(
    band_number,
    expected_state,
    dish_manager,
    event_store_class,
):
    # convert expected state to have underscore for enums in utils
    expected_state = expected_state.replace("-", "_")
    dm_cap_state_events = event_store_class()
    dish_manager.subscribe_event(
        f"b{band_number}CapabilityState",
        tango.EventType.CHANGE_EVENT,
        dm_cap_state_events,
    )
    dm_cap_state_events.wait_for_value(CapabilityStates[expected_state])

    b_x_capability_state = retrieve_attr_value(dish_manager, f"b{band_number}CapabilityState")
    assert b_x_capability_state == expected_state
    LOGGER.info(f"{dish_manager} b{band_number}CapabilityState: {b_x_capability_state}")
